import React, {Component} from 'react';

import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import TodoList from '../todo-list';
import ItemStatusFilter from '../item-status-filter';
import ItemAddForm from '../item-add-form';

import './app.css';

export default class App extends Component {

    maxId = 0;

    state = {
        todoData: [
            this.createTodoItem('Написать приложение')
        ],
        term: '',
        filter: 'all'
    };

    createTodoItem(label) {
        return {
            label: label,
            important: false,
            done: false,
            id: this.maxId++
        }
    }

    deleteItem = (id) => {
        this.setState(({todoData}) => {
            const i = todoData.findIndex((item) => item.id === id);
            return {
                todoData: todoData.filter((item, index) => index !== i)
            }
        })
    };

    addItem = (text) => {
        const newItem = this.createTodoItem(text);

        this.setState(({todoData}) => {
            return {
                todoData: todoData.concat(newItem)
            }
        })
    };

    toggleProperty(arr, id, propName) {
        const i = arr.findIndex((item) => item.id === id);
        const oldItem = arr[i];
        const newItem = {...oldItem, [propName]: !oldItem[propName]};
        return [
            ...arr.slice(0, i),
            newItem,
            ...arr.slice(i + 1)
        ];
    }

    onToggleImportant = (id) => {
        this.setState(({todoData}) => {
            return {
                todoData: this.toggleProperty(todoData, id, 'important')
            }
        })
    };

    onToggleDone = (id) => {
        this.setState(({todoData}) => {
            return {
                todoData: this.toggleProperty(todoData, id, 'done')
            }
        })
    };

    search(array, term) {
        if (!term.length) {
            return array;
        }
        return array.filter((item) => item.label.toLowerCase().indexOf(term.toLowerCase()) > -1)
    }

    onSearchChange = (term) => {
        this.setState({term})
    };

    static filter(array, filter) {
        switch (filter) {
            case 'all':
                return array;
            case 'done':
                return array.filter((item) => item.done);
            case 'active':
                return array.filter((item) => !item.done);
            default:
                return array;
        }
    }

    onFilterChange = (filter) => {
        this.setState({filter})
    };

    render() {
        const {todoData, term, filter} = this.state;
        const visibleItem = App.filter(this.search(todoData, term), filter);
        const doneCount = todoData.filter((item) => item.done).length;
        const todoCount = todoData.length - doneCount;

        return (
            <div className='todo-app'>
                <AppHeader toDo={todoCount} done={doneCount}/>
                <div className='top-panel d-flex'>
                    <SearchPanel onSearchChange={this.onSearchChange}/>
                    <ItemStatusFilter filter={filter}
                                      onFilterChange={this.onFilterChange}/>
                </div>

                <TodoList doList={visibleItem}
                          onDeleted={this.deleteItem}
                          onToggleDone={this.onToggleDone}
                          onToggleImportant={this.onToggleImportant}
                />
                <ItemAddForm addItem={this.addItem}/>
            </div>
        );
    }
};