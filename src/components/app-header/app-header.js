import React from 'react';
import './app-header.css';

const AppHeader = ({toDo, done}) => {
  return (
    <div className='app-header d-flex'>
      <h1>Дела</h1>
      <h2>{toDo} осталось, {done} выполнено</h2>
    </div>
  );
};

export default AppHeader;